"Fișiere_Android" conține fișierele esențiale ale aplicației mobile,
"Fișiere_Romarg" conține fișierele care sunt încărcate pe platforma Romarg,
"Thehouseofplants" este proiectul compilat în Android Studio,
"lolin" conține codul încărcat pe placa Lolin,
"mega" conține codul încărcat pe placa Mega
• Aplicația propusă se concentrează pe monitorizarea parametrilor interiori a unei sere, inclusiv temperatura și umiditatea aerului, precum și umiditatea solului, care sunt critice pentru creșterea și dezvoltarea plantelor. Pentru a obține aceste date, se vor utiliza senzori precum DHT22 pentru temperatura și umiditatea aerului, iar pentru umiditatea solului se va utiliza un alt senzor specializat.
• Datele colectate de la senzori sunt preluate de o placă Arduino Mega, care este un microcontroller puternic și versatil. Aceasta se conectează la modulul Wi-Fi Lolin32, care poate transmite datele la distanță prin intermediul unei rețele wireless. Datele sunt trimise către platforma Romarg pentru stocare și procesare ulterioară.
• Datele sunt stocate într-o bază de date și ulterior prelucrate și vizualizate într-o aplicație web/mobilă. 
• De asemenea, datele sunt transmise către un canal în platforma ThingSpeak pentru a putea fi accesate și analizate ulterior.
• În general, soluția propusă oferă o metodă eficientă și precisă de a monitoriza parametrii interiori ai unei sere, precum și o metodă de stocare și procesare a acestor date. Aceasta poate fi folosită pentru a optimiza creșterea și dezvoltarea plantelor în seră și poate ajuta la reducerea costurilor și a riscurilor asociate cu întreținerea unei sere.

