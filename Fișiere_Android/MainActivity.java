package com.example.thehouseofplants;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get the TextViews by ids
        TextView temperatureValueTextView = findViewById(R.id.temperatureValueTextView);
        TextView humidityValueTextView = findViewById(R.id.humidityValueTextView);
        TextView soilMoistureValueTextView = findViewById(R.id.soilMoistureValueTextView);
        TextView timeValueTextView = findViewById(R.id.timeValueTextView);

        String URL = "https://grandsmartphone.ro/the-house-of-plants/get_database_android.php";
        // Creates a new GET request.
        // Parameters:
        // URL - URL to fetch the string at
        StringRequest stringRequest = new StringRequest(URL,
                //response - Listener to receive the String response
            response -> {
                try {
                    // Retrieve the measurements
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject finalJsonObject = jsonArray.getJSONObject(i);

                        // Set the retrieved values to the corresponding TextViews
                        temperatureValueTextView.setText(finalJsonObject.getString("temperature"));
                        humidityValueTextView.setText(finalJsonObject.getString("humidity"));
                        soilMoistureValueTextView.setText(finalJsonObject.getString("soil_moisture"));
                        timeValueTextView.setText(finalJsonObject.getString("time"));
                        }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            },
                //error - Error listener, or null to ignore errors
            error -> Log.e("Error", error.toString()));

        // Creates a default instance of the worker pool and calls RequestQueue.start() on it.
        // Parameters:
        // context - A Context to use for creating the cache dir.
        // Returns:
        // A started RequestQueue instance.
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        //Adds a Request to the dispatch queue.
        //Parameters:
        //request - The request to service
        requestQueue.add(stringRequest);
    }
}

