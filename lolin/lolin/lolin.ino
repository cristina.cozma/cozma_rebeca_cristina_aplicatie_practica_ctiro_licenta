#include <WiFi.h>// Allows the Arduino board to connect to the internet
#include <SoftwareSerial.h>// Allows data transfer between Arduino and Lolin32 
#include <ArduinoJson.h>// Used for parsing the json object received from Arduino
#include "ThingSpeak.h"// Allows you to send data to the ThinkSpeak channel
#include <HTTPClient.h>// Allows you to make HTTP GET, POST and PUT requests to a web server

const char* ssid    = "Orange-2pbE-2.4G";// Network's name             
const char* password = "9BHV2bB5";// Network's password

SoftwareSerial lolin32(RX, TX);// This line creates an instance of the SoftwareSerial class called lolin32 (RX=Rx & TX=Tx)

WiFiClient  client;// Creates a client capable of connecting to a specified internet IP address
unsigned long myChannelNumber = 2105543;// ThinkSpeak channel id
const char * myWriteAPIKey = "93Z49F020G18VVD8";// ThinkSpeak write API Key

const char* serverName = "https://grandsmartphone.ro/the-house-of-plants/post_database.php";// Domain name and URL path 
String apiKeyValue = "tPmAT5Ab3j7F9";// Keep this API Key value to be compatible with the PHP code provided in the PHP file

// Setup code (runs once)
void setup() {

  Serial.begin(9600);// Lolin is getting ready to communicate with the Serial Monitor, operating at a baud rate of 9600 bits per second.
    
  WiFi.begin(ssid, password);// This function initializes the network settings of the WiFi library and provides the current status
  
   while(WiFi.status() != WL_CONNECTED) {// We check to see if the connection was made  
       delay(500);// Wait 500 ms
       Serial.print(".");
   }
  // We display a message to indicate that the connection was made successfully
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());// Prints the local IP address

  lolin32.begin(9600);// Start and initialize the SoftwareSerial object (the data is going to be sent at a data rate of 9600 bits per second)

  ThingSpeak.begin(client);  // Initialize ThingSpeak
}

// Main code (runs repeatedly)
void loop() {
  StaticJsonBuffer<1000> jsonBuffer;// It handles the memory management and calls the parser (has a fixed size)
  JsonObject& data = jsonBuffer.parseObject(lolin32);// Parses a JSON input and returns a JsonVariant which can contain an array or an object
  if (data == JsonObject::invalid()) {// We check to see if the Json object is invalid
    //Serial.println("Invalid Json Object");
    jsonBuffer.clear();// Resets the JsonBuffer (size goes back to zero)
    return;// Execute loop function code from beginning
  }
  // Prints humidity, temperature and soil moisture received values to the Serial Monitor
  Serial.println("JSON Object Recieved");
  Serial.print("Recieved Humidity:  ");
  float hum = data["humidity"];
  Serial.println(hum);
  Serial.print("Recieved Temperature:  ");
  float temp = data["temperature"];
  Serial.println(temp);
  Serial.print("Recieved Soil moisture:  ");
  int soil_moisture = data["soil moisture"];
  Serial.println(soil_moisture);
  Serial.println("-----------------------------------------");

  // Set the fields with the values
  ThingSpeak.setField(1, hum);
  ThingSpeak.setField(2, temp);
  ThingSpeak.setField(3, soil_moisture);

  // Write to the ThingSpeak channel
  int x = ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
  if(x == 200){
    Serial.println("Channel update successful.");
  }
  else{
    Serial.println("Problem updating channel. HTTP error code " + String(x));
  }

  Serial.println("-----------------------------------------");

  HTTPClient http;// Create an instance of the HTTPClient class
  http.begin(serverName);// Initialize HTTP request
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");// Specify content-type header

  String httpRequestData = "api_key=" + apiKeyValue +  "&temperature=" + temp + "&humidity=" + hum +  "&soil_moisture=" + soil_moisture + "";// HTTP POST request data
  int httpResponseCode = http.POST(httpRequestData);// Send HTTP POST request

  if (httpResponseCode==200) {// Check if the respone code is 200
   Serial.println("HTTP Response code: 200");
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }

  http.end();// Free resources

  Serial.println("-----------------------------------------");

}
