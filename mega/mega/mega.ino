#include <DHT.h> // Used for readig data from the dht sensor
#include <SoftwareSerial.h>// Allows data transfer between Arduino and Lolin32 
#include <ArduinoJson.h>// Used for sending data to Lolin32

#define DHTPIN A1// The pin used to transmit data by the temperature and humidity sensor
#define SOILMOISTUREPIN A0// The pin used to transmit data by the soil moisture sensor
DHT dht(DHTPIN, DHT22);// This line creates an instance of the dht class called DHT
SoftwareSerial lolin32(10, 11);// This line creates an instance of the SoftwareSerial class called lolin32 (10=Rx & 11=Tx)

// Setup code (runs once)
void setup() {
  Serial.begin(9600);// Arduino is getting ready to communicate with the Serial Monitor, operating at a baud rate of 9600 bits per second.
  dht.begin();// Start and initialize the dht object
  lolin32.begin(9600);// Start and initialize the SoftwareSerial object (the data is going to be sent at a data rate of 9600 bits per second)
  delay(1000);// Wait 1 s
}

// Main code (runs repeatedly)
void loop() {
  
  float hum = dht.readHumidity();// Reads the humidity
  float temp = dht.readTemperature();// Reads the temperature
  int soilMoisture= analogRead(SOILMOISTUREPIN);// Reads the soil moisture value
  
  // Prints humidity, temperature and soil moisture values to the Serial Monitor
  Serial.print("Humidity: ");
  Serial.println(hum);
  Serial.print("Temperature: ");
  Serial.println(temp);
  // The sensor value range
  // < 500 is too wet
  // 500-750 is the target range
  // > 750 is dry enough to be watered
  Serial.print("Soil moisture: ");
  Serial.println(soilMoisture);

  StaticJsonBuffer<1000> jsonBuffer;// It handles the memory management and calls the parser (has a fixed size)
  JsonObject& data = jsonBuffer.createObject();// Creates an Json object called data for temperature, humidity and air quality

  // Assign collected data to JSON Object
  data["humidity"] = hum;
  data["temperature"] = temp; 
  data["soil moisture"] = soilMoisture;

  data.printTo(lolin32); // Send data to Lolin32
  jsonBuffer.clear();// Resets the JsonBuffer (size goes back to zero)

  delay(15000);// Wait 15 s
}
