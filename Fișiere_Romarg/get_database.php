<!DOCTYPE html>
<html>
  <head>
    <title>The House of Plants</title>
    <style>
      body {
        background-image: url("background.jpg");
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
        font-family: Arial, sans-serif;
        margin: 0;
      }

      .container {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        min-height: 100vh;
        text-align: center;
        padding: 2rem;
      }

      .title {
        font-size: 7rem;
        color: white;
        margin-bottom: 1rem;
        text-shadow: 2px 2px 5px rgba(0, 0, 0, 0.5);
      }

      .subtitle {
        font-size: 2rem;
        color: white;
        margin-bottom: 2rem;
        text-shadow: 2px 2px 5px rgba(0, 0, 0, 0.5);
      }

      .measurements {
        display: grid;
        grid-template-columns: repeat(2, 1fr);
        grid-gap: 2rem;
        margin-top: 2rem;
      }

      .measurement {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        background-color: rgba(255, 255, 255, 0.8);
        padding: 2rem;
        box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.2);
        border-radius: 5px;
        backdrop-filter: blur(10px);
      }

      .measurement h3 {
        font-size: 1.5rem;
        margin: 0;
        margin-bottom: 1rem;
      }

      .measurement p {
        font-size: 2rem;
        margin: 0;
        font-weight: bold;
        color: #3cb371;
      }
    </style>
  </head>
  <body>
    <?php

    $server_name = "localhost";
    
    $db_name = "r64511gran_date_senzori";// Database name
    
    // User that can access the database
    $username = "r64511gran_raluca";
    
    $password = "z93VuZHqr2vbTwd";
    
    // Create connection
    $conn = new mysqli($server_name, $username, $password, $db_name);
    
    // Check connection
    if ($conn->connect_error)// connect_error function returns a string that describes the error. NULL if no error occurred
    {
        die("Eroare conexiune: " . $conn->connect_error);// Print a message and terminate the current script:
    } 
    
    $sql = "SELECT temperature, humidity, soil_moisture, time FROM Data_from_sensors WHERE time=(
    SELECT MAX(time) FROM Data_from_sensors )";
     
    if ($result = $conn->query($sql)) {// query function - for successful SELECT, SHOW, DESCRIBE, or EXPLAIN queries it will return a mysqli_result object. For other successful queries it will return TRUE. FALSE on failure 
        while ($row = $result->fetch_assoc()) {// fetch_assoc() fetches a result row as an associative array
            $temperature_value = $row["temperature"];
            $humidity_value = $row["humidity"];
            $soil_moisture_value = $row["soil_moisture"];
            $time_value = $row["time"];
        }
        $result->free();// Free the memory associated with the result
    }
    
    $conn->close();// Closes a previously opened database connection
?> 
    <div class="container">
      <h1 class="title">The House of Plants</h1>
      <h2 class="subtitle">The Last Measurements Taken</h2>
      <div class="measurements">
        <div class="measurement">
          <h3>Temperature</h3>
          <p><?php echo $temperature_value; ?></p>
        </div>
        <div class="measurement">
          <h3>Humidity</h3>
          <p><?php echo $humidity_value; ?></p>
        </div>
        <div class="measurement">
          <h3>Soil Moisture</h3>
          <p><?php echo $soil_moisture_value; ?></p>
        </div>
        <div class="measurement">
          <h3>Date and Time</h3>
          <p><?php echo $time_value; ?></p>
        </div>
      </div>
    </div>
  </body>
</html>
